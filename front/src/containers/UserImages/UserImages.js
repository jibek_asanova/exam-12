import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, CircularProgress, Grid, Typography} from "@material-ui/core";
import {fetchUsersPhotosRequest} from "../../store/actions/photosActions";
import ImageItem from "../../components/ImageItem/ImageItem";
import {Link} from "react-router-dom";

const UserImages = ({match}) => {
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);
    const fetchLoading = useSelector(state => state.photos.fetchLoading);
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        dispatch(fetchUsersPhotosRequest(match.params.id));
    }, [dispatch, match.params.id]);

    let username = 'photos';
    let userId = 'id'
    if(photos.length > 0) {
         username = photos[0].userId.displayName;
         userId = photos[0].userId._id
    }
    const isMyImage = user && user._id === userId;

    return (
       <>
           <Grid container direction="column" spacing={2}>
               <Grid item container justifyContent="space-between" alignItems="center">
                   <Grid item>
                       <Typography variant="h4">gallery {username}</Typography>
                   </Grid>
                   {user && user._id === userId && <Button color="inherit" component={Link} to="/photos/add">Add</Button>}
               </Grid>
               <Grid item>
                   <Grid item container direction="row" spacing={1}>
                       {fetchLoading ? (
                           <Grid container justifyContent="center" alignItems="center">
                               <Grid item>
                                   <CircularProgress/>
                               </Grid>
                           </Grid>
                       ) : photos.map(photo => (
                           <ImageItem
                               key={photo._id}
                               id={photo._id}
                               title={photo.title}
                               image={photo.image}
                               isMyImage={isMyImage}
                           />
                       ))}
                   </Grid>
               </Grid>
           </Grid>
       </>
    );
};

export default UserImages;