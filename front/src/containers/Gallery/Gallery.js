import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchPhotosRequest} from "../../store/actions/photosActions";
import ImageItem from "../../components/ImageItem/ImageItem";
import {CircularProgress, Grid, Typography} from "@material-ui/core";

const Gallery = () => {
    const dispatch = useDispatch();
    const photos = useSelector(state => state.photos.photos);
    const fetchLoading = useSelector(state => state.photos.fetchLoading);

    useEffect(() => {
        dispatch(fetchPhotosRequest());
    }, [dispatch]);

    return (
        <>
            <Grid container direction="column" spacing={2}>
                <Grid item container justifyContent="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="h4">Gallery</Typography>
                    </Grid>
                </Grid>
                <Grid item>
                    <Grid item container direction="row" spacing={1}>
                        {fetchLoading ? (
                            <Grid container justifyContent="center" alignItems="center">
                                <Grid item>
                                    <CircularProgress/>
                                </Grid>
                            </Grid>
                        ) : photos.map(photo => (
                            <ImageItem
                                key={photo._id}
                                id={photo.userId._id}
                                title={photo.title}
                                image={photo.image}
                                user={photo.userId.displayName}
                            />
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        </>
    );
};

export default Gallery;