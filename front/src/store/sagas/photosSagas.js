import axiosApi from "../../axiosApi";
import {
    createPhotoFailure, createPhotoRequest,
    createPhotoSuccess, deletePhotoFailure, deletePhotoRequest, deletePhotoSuccess,
    fetchPhotosFailure,
    fetchPhotosRequest,
    fetchPhotosSuccess, fetchUsersPhotosFailure, fetchUsersPhotosRequest, fetchUsersPhotosSuccess
} from "../actions/photosActions";
import {put, takeEvery} from "redux-saga/effects";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";

export function* createPhotoSagas({payload: photoData}) {
    try {
        yield axiosApi.post('/photos', photoData);
        yield put(createPhotoSuccess());
        historyPush('/');
        toast.success('photo created');
    } catch (e) {
        yield put(createPhotoFailure(e.response.data));
        toast.error('Could not create photo');
    }
}

export function* fetchPhotosSagas() {
    try {
        const response = yield axiosApi.get('/photos');
        yield put(fetchPhotosSuccess(response.data));
    } catch (error) {
        if (error.response && error.response.data) {
            yield put(fetchPhotosFailure(error.response.data));
            toast.error(`error: ${error.response.data.error}`);
        } else {
            yield put(fetchPhotosFailure({global: 'No internet'}));
            toast.error('No internet');
        }
    }
}

export function* fetchUsersPhotosSagas({payload: id}) {
    try {
        const response = yield axiosApi.get('/photos/' + id);
        yield put(fetchUsersPhotosSuccess(response.data))
    } catch (error) {
        if (error.response && error.response.data) {
            yield put(fetchUsersPhotosFailure(error.response.data));
            toast.error(`error: ${error.response.data.error}`);
        } else {
            yield put(fetchUsersPhotosFailure({global: 'No internet'}));
            toast.error('No internet');
        }
    }
}

export function* deletePhoto(id) {
    try {
        yield axiosApi.delete('photos/' + id.payload);
        yield put(deletePhotoSuccess(id.payload));
        toast.success('Photo deleted');
    } catch (error) {
        yield put(deletePhotoFailure(error.response.data));
        toast.error('Could not delete photo');
    }
}

const photosSaga = [
    takeEvery(createPhotoRequest, createPhotoSagas),
    takeEvery(fetchPhotosRequest, fetchPhotosSagas),
    takeEvery(fetchUsersPhotosRequest, fetchUsersPhotosSagas),
    takeEvery(deletePhotoRequest, deletePhoto),
];

export default photosSaga;