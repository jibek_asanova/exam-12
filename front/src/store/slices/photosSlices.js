import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
    fetchLoading: false,
    photos: [],
    createPhotoLoading: false,
    createPhotoError: null,
};

const name = 'photos';


const photosSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchUsersPhotosRequest(state) {
            state.fetchLoading = true;
        },
        fetchUsersPhotosSuccess(state, {payload: photos}) {
            state.fetchLoading = false;
            state.photos = photos;
        },
        fetchUsersPhotosFailure(state) {
            state.fetchLoading = false;
        },
        createPhotoRequest(state) {
            state.createPhotoLoading = true;
        },
        createPhotoSuccess(state) {
            state.createPhotoLoading = false;
            state.createPhotoError = null;
        },
        createPhotoFailure(state, {payload: error}) {
            state.createPhotoLoading = false;
            state.createPhotoError = error;
        },
        fetchPhotosRequest(state) {
            state.fetchLoading = true;
        },
        fetchPhotosSuccess(state, {payload: photos}) {
            state.fetchLoading = false;
            state.photos = photos;
        },
        fetchPhotosFailure(state) {
            state.fetchLoading = false;
        },
        deletePhotoRequest(state) {
            state.fetchLoading = true;
        },
        deletePhotoSuccess(state, {payload: photoId}) {
            state.fetchLoading = false;
            state.photos = state.photos.filter(photo => photo._id !== photoId);
        },
        deletePhotoFailure(state) {
            state.fetchLoading = false;
        },
    }
});

export default photosSlice;
