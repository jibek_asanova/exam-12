import photosSlice from "../slices/photosSlices";

export const {
    fetchPhotosRequest,
    fetchPhotosSuccess,
    fetchPhotosFailure,
    createPhotoRequest,
    createPhotoSuccess,
    createPhotoFailure,
    fetchUsersPhotosRequest,
    fetchUsersPhotosSuccess,
    fetchUsersPhotosFailure,
    deletePhotoRequest,
    deletePhotoSuccess,
    deletePhotoFailure
} = photosSlice.actions;