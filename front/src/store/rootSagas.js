import {all} from 'redux-saga/effects';
import usersSaga from "./sagas/usersSagas";
import photosSaga from "./sagas/photosSagas";

export function* rootSagas() {
  yield all([
      ...usersSaga,
      ...photosSaga,
  ])
}