import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import Gallery from "./containers/Gallery/Gallery";
import AddImage from "./containers/AddImage/AddImage";
import {useSelector} from "react-redux";
import UserImages from "./containers/UserImages/UserImages";

const App = () => {
  const user = useSelector(state => state.users.user);

  const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ?
        <Route {...props}/> :
        <Redirect to={redirectTo}/>
  };

  return (
      <Layout>
        <Switch>
          <ProtectedRoute
              path="/photos/add"
              exact
              component={AddImage}
              isAllowed={user}
              redirectTo="/login"
          />
            <Route path="/" exact component={Gallery}/>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
            <Route path="/users/:id" component={UserImages}/>
        </Switch>
      </Layout>
  );
};

export default App;
