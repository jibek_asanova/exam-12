import {
    Backdrop,
    Button,
    Card,
    CardActions,
    CardHeader,
    CardMedia,
    Fade,
    Grid, IconButton,
    makeStyles,
    Modal
} from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';
import {apiURL} from "../../config";
import React, {useState} from "react";
import {Link, useParams} from "react-router-dom";
import {deletePhotoRequest} from "../../store/actions/photosActions";
import {useDispatch} from "react-redux";


const useStyles = makeStyles((theme) => ({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        position: 'relative',
        height: '500px',
        width: '800px',
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(4, 5, 4),
        display: 'inline-block'
    },
    photo: {
        height: '90%',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '20px',

        '& img': {
            height: '100%',
            width: '100%',
            objectFit: 'contain'
        }
    },
    close: {
        position: 'absolute',
        right: '20px',
        top: '20px',

    }
}))

const ImageItem = ({title, id, image, user, isMyImage}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const match = useParams();
    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const cardImage = apiURL + '/' + image;

    return (
        <>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <div className={classes.photo}><img src={cardImage} alt="gallery"/></div>
                        <IconButton onClick={handleClose} className={classes.close}>
                            <CloseIcon/>
                        </IconButton>
                    </div>
                </Fade>
            </Modal>
            <Grid item xs={12} sm={6} md={3} lg={3}>
                <Card className={classes.card}>
                    <CardMedia
                        image={cardImage}
                        className={classes.media}
                        onClick={handleOpen}
                    />
                    <CardHeader title={title}/>
                    {!match.id && <CardActions>
                        <strong>Photo by:</strong>
                        <Button component={Link} to={`/users/${id}`} color="primary" >{user}</Button>
                    </CardActions>}
                    <CardActions>
                        {isMyImage && <Button onClick={() => dispatch(deletePhotoRequest(id))}>delete</Button>}
                    </CardActions>

                </Card>

            </Grid>
        </>

    );
};

ImageItem.propTypes = {
    title: PropTypes.string.isRequired,
    image: PropTypes.string
}

export default ImageItem;