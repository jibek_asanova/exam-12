const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Photo = require('../models/Photo');
const auth = require("../middleware/auth");


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const photos = await Photo.find().populate('userId', 'displayName');
    res.send(photos);
  } catch (e) {
    res.status(500).send({error: 'Internal Server Error'});
  }
});

router.get('/:id', async (req, res) => {
  try {
    const photo = await Photo.find({userId: req.params.id}).populate('userId', 'displayName');

    if (photo) {
      res.send(photo);
    } else {
      res.status(404).send({error: 'Photo not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});


router.post('/', auth,  upload.single('image'), async (req, res) => {
  try {
    const photoData = {
      title: req.body.title,
      userId: req.user._id,
    };

    if (req.file) {
      photoData.image = 'uploads/' + req.file.filename
    }

    const photo = new Photo(photoData);
    await photo.save();
    res.send(photo);

  } catch (error) {
    res.status(400).send(error);
  }
});

router.delete('/:id', auth, async (req, res) => {
  try {
    const photo = await Photo.findByIdAndDelete(req.params.id);

    if (photo) {
      res.send(`Photo ${photo.title} removed`);
    } else {
      res.status(404).send({error: 'Photo not found'})
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


module.exports = router;