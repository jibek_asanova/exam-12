const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Photo = require("./models/Photo");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }
  const [user1, user2] = await User.create({
      email: 'jibek@gmail.com',
      password: '123',
      token: nanoid(),
      displayName: 'Jibek',
      avatarImage: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1obN7zovv6kbr6dDQcf26ZJHFbKOCDXjMxVZh0LLrSenzaxL2'
    }, {
      email: 'bob@gmail.com',
      password: '555',
      token: nanoid(),
      displayName: 'Bob',
      avatarImage: 'https://upload.wikimedia.org/wikipedia/commons/4/48/Rapper_B.o.B_2013.jpg'
    },
  )

  await Photo.create(
    {
      title: 'photo 1',
      image: 'fixtures/photo1.jpeg',
      userId: user1,
    }, {
      title: 'photo 2',
      image: 'fixtures/photo2.jpg',
      userId: user2,
    }, {
      title: 'photo3',
      image: 'fixtures/photo3.jpg',
      userId: user1,
    })

  await mongoose.connection.close();

};

run().catch(console.error)